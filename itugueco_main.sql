-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 10, 2018 at 05:10 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itugueco_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `tconreg_day`
--

CREATE TABLE `tconreg_day` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `day` tinyint(3) NOT NULL,
  `title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tconreg_day`
--

INSERT INTO `tconreg_day` (`id`, `date`, `day`, `title`) VALUES
(2, '2018-04-26', 1, 'Opening seminar with different topics in tech'),
(3, '2018-05-26', 2, 'training hands on on angular and react js. lol'),
(4, '2018-04-28', 3, 'Beach Please event');

-- --------------------------------------------------------

--
-- Table structure for table `tconreg_merch`
--

CREATE TABLE `tconreg_merch` (
  `id` int(11) NOT NULL,
  `merch` varchar(150) NOT NULL,
  `price` float NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tconreg_merch`
--

INSERT INTO `tconreg_merch` (`id`, `merch`, `price`, `status`) VALUES
(1, 'Small t-shirt', 300, 1),
(2, 'medium t-shirt', 300, 1),
(3, 'Large t-shirt', 350, 1),
(4, 'Extra large t-shirt', 350, 1),
(5, 'xxlarge', 350, 0),
(6, 'xxxlarge', 350, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tconreg_rcodes`
--

CREATE TABLE `tconreg_rcodes` (
  `id` int(11) NOT NULL,
  `rcode` varchar(10) NOT NULL,
  `owner` varchar(150) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tconreg_rcodes`
--

INSERT INTO `tconreg_rcodes` (`id`, `rcode`, `owner`, `status`) VALUES
(1, 'xnxx', 'Ryan torrado', 1),
(2, 'xxxx', 'Elton Bagne', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tconreg_regday`
--

CREATE TABLE `tconreg_regday` (
  `id` int(11) NOT NULL,
  `regid` int(11) NOT NULL,
  `dayid` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tconreg_regday`
--

INSERT INTO `tconreg_regday` (`id`, `regid`, `dayid`, `status`) VALUES
(24, 1286, 2, 0),
(25, 1286, 3, 0),
(26, 1286, 4, 0),
(27, 1287, 2, 0),
(28, 1287, 3, 0),
(29, 1281, 2, 0),
(30, 1288, 2, 0),
(31, 1288, 3, 0),
(32, 1288, 4, 0),
(33, 1289, 2, 0),
(34, 1282, 2, 0),
(35, 1282, 3, 0),
(36, 1282, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tconreg_registrant`
--

CREATE TABLE `tconreg_registrant` (
  `id` int(11) NOT NULL,
  `lname` varchar(70) NOT NULL,
  `fname` varchar(70) NOT NULL,
  `mname` varchar(70) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(13) NOT NULL,
  `category` varchar(12) NOT NULL,
  `agency` varchar(250) NOT NULL,
  `rcode` varchar(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tconreg_registrant`
--

INSERT INTO `tconreg_registrant` (`id`, `lname`, `fname`, `mname`, `email`, `mobile`, `category`, `agency`, `rcode`, `status`, `code`) VALUES
(1243, 'Babaran', 'Carlos Jr.', 'Lucena', '', '09167895103', 'Student', 'SPUP', '', 0, 'yrb3j'),
(1246, 'Jansalin', 'Jan Jacob ', 'Martinez', '', '09258867026', 'Professional', 'University of the Philippines Los Banos', '', 0, 'sxlv3'),
(1250, 'Orlanes', 'Ian Kristopher', 'Chua', 'Orlanesian@gmail.com', '09360363083', 'Professional', 'LGU Tuguegarao', '', 0, 'ck7wj'),
(1251, 'Menor', 'Reymart', 'Solancho', 'reymartmenormart@gmail.com', '09158672048', 'Professional', 'unemployed', '', 0, 'paa4c'),
(1252, 'Menor', 'Reymart', 'Solancho', 'reymartmenormart@gmail.com', '09158672048', 'Professional', 'Unemployed', '', 0, 'fn5v2'),
(1253, 'Apostol', 'Mark Anthony', 'Allam', 'csu.mark2014@gmail.com', '09752324160', 'Professional', 'Golden Press Tuguegarao', '', 0, 'icxa0'),
(1254, 'Masong', 'Earl Japeth', '', 'japethmasong@gmail.com', '09263905453', 'Professional', 'sc88 technologies', '', 0, 'dxksk'),
(1255, 'Guzman', 'Danniver', 'Raful', 'danniverguzman@gmail.com', '09175993032', 'Professional', 'Cagayan State University', '', 0, '4nj97'),
(1257, 'qwe', 'qweqw', 'qwewq', 'qweqwew@gmail.com', '0123912302398', 'Student', 'eqwewqe', '3123', 0, '2ls5e'),
(1258, 'Apostol', 'Mark Anthony', 'Allam', 'csu.mark2014@gmail.com', '09752324160', 'Professional', 'Golden Press Tuguegarao', '', 0, 'ijyli'),
(1259, 'Apostol', 'Mark Anthony', 'Allam', 'csu.mark2014@gmail.com', '09752324160', 'Professional', 'Golden Press Tuguegarao', '', 0, 'l1d6o'),
(1271, 'Rubios', 'Jefferson', 'Villanueva', 'jeffrubios@gmail.com', '09997080159', 'Professional', 'Fujitsu', '', 0, 'u7kyd'),
(1272, 'Macapia', 'Glynish', '', 'glynishlmacapia@gmail ', '09266959481', 'Professional', 'University of Cagayan Valley', '', 0, '7l8yh'),
(1273, 'Macapia', 'Glynish', '', 'glynishlmacapia@gmail ', '09266959481', 'Professional', 'University of Cagayan Valley', '', 0, 'rjsg5'),
(1274, 'Solangon', 'Daryl', 'Sumaylo', 'Dsolangon@gmail.com', '639178479004', 'Professional', 'Fujitsu Philippines Inc', '', 0, '45pl0'),
(1275, 'Jansalin', 'Jan Jacob Glenn', 'Martinez', 'j.jansalin@up.edu.ph', '09258867026', 'Professional', 'University of the Philippines Los Banos', '', 0, 'pqwu9'),
(1276, 'Chua', 'Ryan Joseph', 'Ruiz', 'rjchua15@yahoo.com', '09276866224', 'Professional', '@home', '', 0, 'm499v'),
(1277, 'Dalog', 'Reynaldo', 'Reynaldo', 'reynaldo_dalog@hotmail.com', '09273550612', 'Professional', 'Cauayan Medical Specialists Hospital', '', 0, 'i9grh'),
(1278, 'Asuncion', 'Mark Lister', 'Argel', 'marklisterargelasuncion@gmail.com', '09975600093', 'Professional', 'Mallig Plains Rural Bank', '', 0, 'cwh42'),
(1279, 'Anonymous', 'Anonynmous', 'Anonynanmous', 'jzon290@mail.com', '09355940728', 'Student', 'Cagayan State University', '', 0, 'vzu4r'),
(1280, 'alfaro', 'dennis', 'llegado', 'dlalfaro@spuqc.edu.ph', '09228881625', 'Professional', 'St. Paul University Quezon City', '', 0, '1hf45'),
(1281, 'Tangan', 'NiÃ±a Jane', 'Allam', 'ninajanetangan6@gmail.com', '09357418446', 'Student', 'University of Saint Louis Tuguegarao', '', 0, '13rgz'),
(1282, 'Mendoza', 'Aaron', '', 'aaronmendoza.official@gmail.com', '', 'Professional', 'Teleperformance Philippines', '', 0, 'ng5l7');

-- --------------------------------------------------------

--
-- Table structure for table `tconreg_regmerch`
--

CREATE TABLE `tconreg_regmerch` (
  `id` int(11) NOT NULL,
  `regid` int(11) NOT NULL,
  `merchid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tconreg_regmerch`
--

INSERT INTO `tconreg_regmerch` (`id`, `regid`, `merchid`, `quantity`, `status`) VALUES
(31, 1281, 4, 1, 0),
(32, 1282, 6, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tconreg_day`
--
ALTER TABLE `tconreg_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tconreg_merch`
--
ALTER TABLE `tconreg_merch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tconreg_rcodes`
--
ALTER TABLE `tconreg_rcodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tconreg_regday`
--
ALTER TABLE `tconreg_regday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tconreg_registrant`
--
ALTER TABLE `tconreg_registrant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tconreg_regmerch`
--
ALTER TABLE `tconreg_regmerch`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tconreg_day`
--
ALTER TABLE `tconreg_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tconreg_merch`
--
ALTER TABLE `tconreg_merch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tconreg_rcodes`
--
ALTER TABLE `tconreg_rcodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tconreg_regday`
--
ALTER TABLE `tconreg_regday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tconreg_registrant`
--
ALTER TABLE `tconreg_registrant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1283;
--
-- AUTO_INCREMENT for table `tconreg_regmerch`
--
ALTER TABLE `tconreg_regmerch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
