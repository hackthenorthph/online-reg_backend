<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap.min.css">
</head>
<body style="margin:10px">

<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
                  include_once 'config/database.php';
                  include_once 'objects/registrant.php';
                  include_once 'objects/days.php';
                  include_once 'objects/merch.php';
                  $database = new Database();
                  $db = $database->getConnection();
                  $proj = new Registrants($db);
                  $stmt = $proj->readall2();
                  $num = $stmt->rowCount();
                  if ($num!=0) { 
                  ?>
                  <button id="expo" onclick="downloadthis();"  class="btn btn-success">Export to Excel</button> <br><br>
                  <table class="table table-bordered " id="table2excel" style="width: 95% !important;">
                    <tr>
                      <th>Fullname</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Category</th>
                      <th>Agency</th>
                      <th>Ceference code</th>
                      <th>Code</th>
                      <th>Day 1</th>
                      <th>Day 2</th>
                      <th>Day 3</th>
                      <th>Tshirt small</th>
                      <th>Tshirt medium</th>
                      <th>Tshirt large</th>
                      <th>Tshirt Xlarge</th>
                      <th>Tshirt XXlarge</th>
                      <th>Tshirt XXXlarge</th>
                      <th>Total</th>
                    </tr>
                    
                  <?php
                  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                  extract($row); 
                  $x=0;
                  ?>
                       <tr>
                         <td>
                          <?php echo $lname.", ".$fname." ".$mname; ?>
                         </td>
                         <td>
                           <?php echo $email; ?>
                         </td>
                         <td>
                           <?php echo $mobile; ?>
                         </td>
                         <td>
                           <?php echo $category; ?>
                         </td>
                         <td>
                           <?php echo $agency; ?>
                         </td>
                         <td>
                           <?php echo $rcode; ?>
                         </td>
                         <td>
                           <?php echo $code; ?>
                         </td>
                         <td data-toggle="tooltip" title="Day 1">
                            <?php
                            $day1 = new Days($db);
                            $stmt2 = $day1->days($id,2);
                            $num2 = $stmt2->rowCount();
                            if ($num2!=0) {
                              if($category=="Student"){
                                $x+=1000;
                              }else{
                                 $x+=1250;
                              }

                              echo "yes";
                            } ?>
                         </td>
                         <td data-toggle="tooltip" title="Day 2">
                            <?php
                            $day1 = new Days($db);
                            $stmt2 = $day1->days($id,3);
                            $num2 = $stmt2->rowCount();
                            if ($num2!=0) {
                              if($category=="Student"){
                                $x+=1000;
                              }else{
                                 $x+=1250;
                              }

                              echo "yes";
                            } ?>
                         </td>
                         <td data-toggle="tooltip" title="Day 3">
                            <?php
                            $day1 = new Days($db);
                            $stmt2 = $day1->days($id,4);
                            $num2 = $stmt2->rowCount();
                            if ($num2!=0) {
                              if($category=="Student"){
                                $x+=1000;
                              }else{
                                 $x+=1250;
                              }

                              echo "yes";
                            }  ?>
                         </td>
                         <td data-toggle="tooltip" title="small">
                            <?php
                            $merc = new Merchs($db);
                            $stmt3 = $merc->read($id,1);
                            $row3 = $stmt3->fetch();
                            $num3 = $stmt3->rowCount();
                            if ($num3!=0) {
                              $x+=300;
                              echo $row3['quantity'];
                            } ?>
                         </td>
                         <td data-toggle="tooltip" title="medium">
                            <?php
                            $merc2 = new Merchs($db);
                            $stmt3 = $merc2->read($id,2);
                            $row3 = $stmt3->fetch();
                            $num3 = $stmt3->rowCount();
                            if ($num3!=0) {
                              $x+=300;
                              echo $row3['quantity'];
                            } ?>
                         </td>
                         <td data-toggle="tooltip" title="large">
                            <?php
                            $merc3 = new Merchs($db);
                            $stmt3 = $merc3->read($id,3);
                            $row3 = $stmt3->fetch();
                            $num3 = $stmt3->rowCount();
                            if ($num3!=0) {
                              $x+=350;
                              echo $row3['quantity'];
                            } ?>
                         </td>
                         <td data-toggle="tooltip" title="Xlarge">
                            <?php
                            $merc4 = new Merchs($db);
                            $stmt3 = $merc4->read($id,4);
                            $row3 = $stmt3->fetch();
                            $num3 = $stmt3->rowCount();
                            if ($num3!=0) {
                              $x+=350;
                              echo $row3['quantity'];
                            } ?>
                         </td>
                         <td data-toggle="tooltip" title="XXlarge">
                            <?php
                            $merc5 = new Merchs($db);
                            $stmt3 = $merc5->read($id,5);
                            $row3 = $stmt3->fetch();
                            $num3 = $stmt3->rowCount();
                            if ($num3!=0) {
                              $x+=350;
                              echo $row3['quantity'];
                            } ?>
                         </td>
                         <td  data-toggle="tooltip" title="XXXlarge">
                            <?php
                            $merc6 = new Merchs($db);
                            $stmt3 = $merc6->read($id,6);
                            $row3 = $stmt3->fetch();
                            $num3 = $stmt3->rowCount();
                            if ($num3!=0) {
                              $x+=350;
                              echo $row3['quantity'];
                            } ?>
                         </td>
                         <td data-toggle="tooltip" title="Total">
                           
                           <?php echo $x; ?>
                         </td>
                       </tr>
                <?php  
                        } echo "</table>";
                }
                ?>
                 <script type="text/javascript">
                             function downloadthis() {
                               // body...
                                    $("#table2excel").table2excel({
                                    exclude: ".noExl",
                                    name: "Excel Document Name",
                                    filename: "exported report",
                                    fileext: ".xls",
                                    exclude_img: true,
                                    exclude_links: true,
                                    exclude_inputs: true
                                  });
                                }
                      </script><script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script src="assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery.table2excel.min.js"></script>

</body>
</html>
