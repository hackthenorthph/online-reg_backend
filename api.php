<?php
// This is the API, 2 possibilities: show the app list or show a specific app by id.
// This would normally be pulled from a database but for demo purposes, I will be hardcoding the return values.

include_once('config/database.php');
include_once('objects/registrant.php');
include_once('objects/days.php');
include_once('objects/merch.php');
                                                    
header("Access-Control-Allow-Origin: *");

header('Content-Type: application/json');


function get_app_list()
{
  $database = new Database();
  $db = $database->getConnection();
  $read = new stayinndata($db);
  $x=0;
  $stmt = $read->read();
    while ($row = $stmt->fetch()) {
      if ($x==0) {
                $array= array("id" => $row['id'],"address" =>  $row['address'], "price" =>  $row['price'], "title" =>  $row['title'], "type" =>  $row['type'], "contact" =>  $row['contact'], "long" =>  (float)$row['long'], "lat" =>  (float)$row['lat'], "picture" =>  $row['picture']  , "thumbnail" =>  $row['thumbnail'], "description" =>  $row['description'] );
              $app_list[$x] =$array;
              $x++;
        
        }
      }
  return $app_list;
}

$database = new Database();
$db = $database->getConnection();
$read = new Registrants($db);
if ($_GET['q']=='reg') {

   $read->lname=$_GET['lname'];
   $read->fname=$_GET['fname'];
   $read->mname=$_GET['mname'];
   $read->mobile=$_GET['mobile'];
   $read->email=$_GET['email'];
   $read->category=$_GET['category'];
   $read->agency=$_GET['agency'];
   $read->rcode=$_GET['rcode'];
   do{
   $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
   $stmt = $read->readcode($s);
   $num = $stmt->rowCount();
   }while ($num>0);
   $read->code=$s;

  if($read->create())
    {

      $stmt = $read->read();
      $row = $stmt->fetch();
      $value= array("message" => $row['code']);

      $day = new Days($db);
      $day->regid=$row['id'];
      if ($_GET['day1']>0) {
        $day->dayid=2;
        $day->reg();
      }
      if ($_GET['day2']>0) {
        $day->dayid=3;
        $day->reg();
      }
      if ($_GET['day3']>0) {
        $day->dayid=4;
        $day->reg();
      }

      $merch = new Merchs($db);
      $merch->regid=$row['id'];
      if ($_GET['s']>0) {
        $merch->merchid=1;
        $merch->quantity=$_GET['s'];
        $merch->reg();
      }
      if ($_GET['m']>0) {
        $merch->merchid=2;
        $merch->quantity=$_GET['m'];
        $merch->reg();
      }
      if ($_GET['l']>0) {
        $merch->merchid=3;
        $merch->quantity=$_GET['l'];
        $merch->reg();
      }
      if ($_GET['xl']>0) {
        $merch->merchid=4;
        $merch->quantity=$_GET['xl'];
        $merch->reg();
      }if ($_GET['xxl']>0) {
        $merch->merchid=5;
        $merch->quantity=$_GET['xxl'];
        $merch->reg();
      }if ($_GET['xxxl']>0) {
        $merch->merchid=6;
        $merch->quantity=$_GET['xxxl'];
        $merch->reg();
      }
    }else
    {
      $value= array("message" => "failed");
    }
  # code...
}

//return JSON array
exit(json_encode($value));
?>